import './styles/index.scss'


function onMenu() {
  const _menuBtn = document.querySelector('#menu-bar')
  const _menu = document.querySelector('.menu')

  _menuBtn.addEventListener('click', e => {
    
    e.preventDefault()
    _menu.classList.toggle('menu_active')
  })
}

onMenu()

