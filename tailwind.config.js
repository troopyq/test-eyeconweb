module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.{js,jsx}"],
  content: [],
  theme: {
    extend: {
      colors: {
        // "gray-100" : "#F3F4F6",
        // "gray-900" : "#111827"
        'primary': '#6BA91A',
        'primary-2': '#64A315',
        'secondary': '#2563EB',
      },
    },
  },
  plugins: [],
}
