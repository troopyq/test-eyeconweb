const cssnanoPlugin = require("cssnano")
const tailwindcss = require("tailwindcss")

module.exports = {
  plugins: [ "autoprefixer", "postcss-preset-env", tailwindcss, cssnanoPlugin({preset: 'default'})]
}