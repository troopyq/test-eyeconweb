const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

let mode = 'development'

if(process.env.NODE_ENV === 'production'){
  mode = 'production'
}

module.exports = {
  entry: './src/index.js',
  mode: mode,
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: 'assets/[hash][ext][query]',
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(s[ac]|c)ss$/i,
        include: path.resolve(__dirname, "src"),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: { publicPath: "" },
          },
          "css-loader",
          "postcss-loader", 
          "sass-loader",
        ],
      },
      {
        test: /\.js$/i,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        type: "asset/resource",
      },
      // {
      //   test: /\.(jpe?g|png|gif|svg)$/i,
      //   loader: 'file-loader',
      //   options: {
      //     name: '[name].[hash].[ext]',
      //     outputPath: 'images',
      //     esModule: false,
      //   },
      // },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: "asset/resource",
      },
      // {
      //   test: /\.(woff|woff2|eot|ttf|otf)$/i,
      //   loader: 'file-loader',
      //   options: {
      //     outputPath: 'assets/fonts',
      //   },
      // },
      {
        test: /\.html$/i,
        use: {
          loader: 'html-loader',
          options: {
              sources: true,
          }
        }
      },
     
    ],
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: 'body',
      template: './src/index.html',
      filename: 'index.html',
    }),
    // new CopyPlugin({
    //   patterns: [{from: "src/index.html", to: "index.html"}],
    // }),
    // new CopyPlugin({
    //   patterns: [{ from: 'src/images', to: "images" }],
    // }),
    new MiniCssExtractPlugin(),
  ],
  devtool: "source-map",
  devServer: {
    watchFiles: ["src/**/*"],
    historyApiFallback: true,
    hot: true,
    
  },
  cache: true,
 
};